/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* gturing.c - a Turing machine simulator.
 * Copyright (C) 1998 The Free Software Foundation
 * Copyright (C) 2001-2006 German Poo-Caaman~o
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <gnome.h>
#include <libgnomecanvas/libgnomecanvas.h>
#include <gconf/gconf-client.h>

#include "turing.h"
#include "gturing.h"
#include "turing_table_editor.h"
#include "graph_editor.h"
#include "turing_table_editor.h"

#define GCONF_FONT	"/desktop/gnome/interface/monospace_font_name"
#define GCONF_SPEED	"/apps/gturing/speed"
#define GCONF_TAPE	"/apps/gturing/tape"
#define GCONF_PROGRAM	"/apps/gturing/program"
#define GCONF_WIDTH  "/apps/gturing/width"
#define GCONF_HEIGHT "/apps/gturing/height"

static void power_do (const gchar *tape);
static void save_callback (GtkWidget *widget, gpointer data);
static gboolean prompt (const gchar *title, const gchar *msg, 
	const gchar *value, gchar **result);
static void open_callback (GtkWidget *widget, gpointer data);
static void tape_callback (GtkWidget *widget, gpointer data);
static void view_comment_callback (GtkWidget *widget, gpointer data);
static void exit_callback (GtkWidget *widget, gpointer data);
static void playspeed_callback (GtkWidget *widget, gpointer data);
static void help_callback (GtkWidget *widget, gpointer data);
static void about_callback (GtkWidget *widget, gpointer data);

static void set_ui_sensitive (gboolean powers, gboolean stops, 
	gboolean plays, gboolean steps);
static void set_tape (const gchar *str);
static void view_comment (void);

static void set_states_fname (const gchar *filename);

static int next_state (turing * t);

static gint do_play (gpointer data);
static void play_callback (GtkWidget * play_button, gpointer data);
static void step_callback (GtkWidget * step_buttton, gpointer data);
static void stop_callback (GtkWidget * stop_button, gpointer data);

static void create_machine (void);
static GtkWidget * init_interface (void);
static void init_globals (gchar *filename);


static char *prog_message = NULL;
static gchar *tape_string;
static gchar *states_fname;

static gint speed = -1;
static gboolean stop_flag = TRUE;

static GtkWidget *main_window;
static GtkWidget *dialog = NULL;
static GtkWidget *entry;

static GtkWidget *vbox;
static GtkWidget *tapelabel;
static GtkWidget *headlabel;
GtkTreeView *treeview_editor = NULL;
#ifdef GRAPH_EDITOR
static GtkWidget *graph_editor = NULL;
#endif

static GnomeAppBar *statusline;

static GConfClient *gengine;

/* Sensitive Actions */
GtkAction *stop_action;
GtkAction *reset_action;
GtkAction *play_action;
GtkAction *step_action;

/* GtkAction and GtkUiManager */
static const GtkActionEntry entries[] = {
  { "FileMenu", NULL, N_("_File") },
  { "ActionMenu", NULL, N_("_Actions") },
  { "SettingsMenu", NULL, N_("_Settings") },
  { "HelpMenu", NULL, N_("_Help") },
  { "Open", GTK_STOCK_OPEN, N_("_Open..."), "<control>O", 
	  N_("Open an existing file"), G_CALLBACK (open_callback) },
  { "SaveAs", GTK_STOCK_SAVE, N_("Save _as..."), "<shift><control>S", 
	  N_("Save the current file with a new name"), 
	  G_CALLBACK (save_callback) },
  { "Comments", NULL, N_("_Comments..."), NULL, 
	  N_("View the program's comment"), 
	  G_CALLBACK (view_comment_callback) },
  { "Exit", GTK_STOCK_QUIT, N_("E_xit"), "<control>Q", 
	  N_("Exit the program"), G_CALLBACK (exit_callback) },
  { "Reset", GTK_STOCK_REFRESH, N_("_Reset"), "<control>R", 
	  N_("Reset the machine going to the first state"), 
	  G_CALLBACK (power_do) },
  { "Pause", GTK_STOCK_STOP, N_("P_ause"), "F3", 
	  N_("Stop the machine."), G_CALLBACK (stop_callback) },
  { "Play", GTK_STOCK_GO_FORWARD, N_("_Play"), "F4", 
	  N_("Start the machine in the current state"), 
	  G_CALLBACK (play_callback) },
  { "Step", GTK_STOCK_GOTO_LAST, N_("_Step"), "F5", 
	  N_("Allow the machine advance step by step from the current state"),
	  G_CALLBACK (step_callback) },
  { "Speed", NULL, N_("_Speed..."), "<control>D", 
	  N_("Set play speed."), G_CALLBACK (playspeed_callback) },
  { "Tape", NULL, N_("_Tape..."), "<control>T", 
	  N_("Set the tape to play"), G_CALLBACK (tape_callback) },
/*  { "Preferences", NULL, N_("P_references"), NULL, 
 *  N_("Personalize your gturing's settings."), 
 *  G_CALLBACK (item_activated) },
 *  */
  { "Index", GTK_STOCK_HELP, N_("_Index"), "F1", 
	  N_("Open the gturing's manual"), G_CALLBACK (help_callback) },
  { "About", GTK_STOCK_ABOUT, N_("_About..."), NULL, 
	  N_("Display information about the program"), 
	  G_CALLBACK (about_callback) },
};

static const char *ui_description =
"<ui>"
"  <menubar name='MainMenu'>"
"    <menu action='FileMenu'>"
"      <menuitem action='Open'/>"
"      <menuitem action='SaveAs'/>"
"      <separator/>"
"      <menuitem action='Comments'/>"
"      <separator/>"
"      <menuitem action='Exit'/>"
"    </menu>"
"    <menu action='ActionMenu'>"
"      <menuitem action='Reset'/>"
"      <menuitem action='Pause'/>"
"      <menuitem action='Play'/>"
"      <menuitem action='Step'/>"
"      <separator/>"
"    </menu>"
"    <menu action='SettingsMenu'>"
"      <menuitem action='Speed'/>"
"      <menuitem action='Tape'/>"
"      <separator/>"
"    </menu>"
"    <menu action='HelpMenu'>"
"      <menuitem action='Index'/>"
"      <menuitem action='About'/>"
"      <separator/>"
"    </menu>"
"  </menubar>"
"  <toolbar name='Toolbar'>"
"    <placeholder name='ActionToolItems'>"
"      <toolitem action='Open'/>"
"      <toolitem action='SaveAs'/>"
"      <separator/>"
"      <toolitem action='Reset'/>"
"      <toolitem action='Pause'/>"
"      <toolitem action='Play'/>"
"      <toolitem action='Step'/>"
"    </placeholder>"
"  </toolbar>"
"</ui>";

static void
set_ui_sensitive (gboolean reset, gboolean stop,
		  gboolean play, gboolean step)
{
	gtk_action_set_sensitive (reset_action, reset);
	gtk_action_set_sensitive (stop_action, stop);
	gtk_action_set_sensitive (play_action, play);
	gtk_action_set_sensitive (step_action, step);
}

static void
set_tape (const gchar *str)
{
	int len, i;
	gchar *tmp;

	if (str == NULL) {
		return;
	}

	len = strlen (str);
	tmp = g_malloc (len + 1);

	for (i = 0; i < len; i++)
		if (i == tm->pos)
			tmp[i] = '^';
		else
			tmp[i] = ' ';

	tmp[len] = 0;

	gtk_label_set_text (GTK_LABEL (tapelabel), str);
	gtk_label_set_text (GTK_LABEL (headlabel), tmp);

	g_free (tmp);
}

/* view_comment: Show the comments gotten from file and show them in
 * a message box */
static void
view_comment (void)
{
	GtkWidget *dialog;
	gchar *message;

	if (prog_message && *prog_message && *prog_message != '\n')
		message = prog_message;
	else
		message = _("No comment for this program.");

	dialog = gtk_message_dialog_new (GTK_WINDOW (main_window),
					 GTK_DIALOG_MODAL |
					 GTK_DIALOG_DESTROY_WITH_PARENT,
					 GTK_MESSAGE_INFO, GTK_BUTTONS_OK,
					 "%s", message);
	gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
}

static void
set_states_fname (const gchar *filename)
{
	gchar *basename;
	gchar *title;
	
	if ((filename != NULL) && (tape_string != NULL)) {
		power_do (tape_string);
	} else {
		set_ui_sensitive (FALSE, FALSE, FALSE, FALSE);
	}
	
	basename = g_path_get_basename (filename);
	title = g_strdup_printf ("%s - %s", PACKAGE, basename);

	gtk_window_set_title (GTK_WINDOW (main_window), title);

	g_free (basename);
	g_free (title);

	turing_table_editor_set_model (treeview_editor, tm);

	if (tape_string != NULL) {
		power_do (tape_string);
	}
}

static int
next_state (turing * t)
{
	int ret;
	char buff[20];

	turing_table_editor_animate (treeview_editor, t);
	ret = turing_run_state (t);
	
	if (t->actualstate) {
		snprintf (buff, 20, _("State: %d"), t->actualstate->no);
		gnome_appbar_set_status (GNOME_APPBAR (statusline), buff);
	} else {
		gnome_appbar_set_status (GNOME_APPBAR (statusline),
					 _("Stopped"));
	}
	return ret;
}

static void
power_do (const gchar *tape)
{
	stop_flag = TRUE;
	tm->state = 0;
	tm->pos = 0;

	turing_set_tape (tm, tape_string);
	set_tape (tape_string);

	next_state (tm);
	set_ui_sensitive (FALSE, FALSE, TRUE, TRUE);
}


static gint
do_play (gpointer data)
{
	char *tmp;
	int cont;

	cont = FALSE;

	if (!stop_flag) {
		tmp = turing_get_tape (tm);
		set_tape (tmp);
		free (tmp);

		cont = next_state (tm);

		if (!cont)
			set_ui_sensitive (TRUE, FALSE, stop_flag,
					  stop_flag);
	}

	return cont;
}

static void
play_callback (GtkWidget *play_button, gpointer data)
{
	set_ui_sensitive (TRUE, TRUE, FALSE, FALSE);

	stop_flag = FALSE;
	g_timeout_add (speed, do_play, NULL);

}

static void
step_callback (GtkWidget *step_buttton, gpointer data)
{
	char *tmp;

	tmp = turing_get_tape (tm);
	set_tape (tmp);
	free (tmp);

	if (!next_state (tm)) {
		set_ui_sensitive (TRUE, FALSE, FALSE, FALSE);
		gnome_appbar_set_status (GNOME_APPBAR (statusline),
					 _("Stopped"));
	}

	set_ui_sensitive (TRUE, FALSE, TRUE, TRUE);
}

static void
stop_callback (GtkWidget *stop_button, gpointer data)
{
	stop_flag = TRUE;

	set_ui_sensitive (TRUE, FALSE, TRUE, TRUE);
}

static gboolean
prompt (const gchar *title, const gchar *msg, const gchar *value, gchar **result)
{
	GtkWidget *vbox, *hbox, *label;
	gint response;

	if (dialog != NULL) {
		(*result) = NULL;
		return FALSE;
	}

	dialog = gtk_dialog_new_with_buttons (title, GTK_WINDOW (main_window),
					      GTK_DIALOG_MODAL |
					      GTK_DIALOG_DESTROY_WITH_PARENT,
					      GTK_STOCK_CANCEL,
					      GTK_RESPONSE_CANCEL,
					      GTK_STOCK_OK,
					      GTK_RESPONSE_OK, NULL);

	vbox = GTK_DIALOG (dialog)->vbox;

	hbox = gtk_hbox_new (FALSE, GNOME_PAD);
	gtk_container_set_border_width (GTK_CONTAINER (hbox), GNOME_PAD);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);

	label = gtk_label_new (msg);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	entry = gtk_entry_new ();
	gtk_entry_set_text (GTK_ENTRY (entry), value);
	gtk_box_pack_start (GTK_BOX (hbox), entry, TRUE, TRUE, 0);

	gtk_widget_show_all (hbox);
	
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);

	response = gtk_dialog_run (GTK_DIALOG (dialog));
	if (response == GTK_RESPONSE_OK) {
		(*result) = g_strdup (gtk_entry_get_text (GTK_ENTRY (entry)));
	} else {
		(*result) = NULL;
	}

	gtk_widget_destroy (dialog);
	dialog = NULL;

	return (response == GTK_RESPONSE_OK);
}

void
save_callback (GtkWidget *widget, gpointer data)
{
	GtkWidget *dialog;

	dialog = gtk_file_chooser_dialog_new (_("Save a Turing Program File"),
				      GTK_WINDOW (main_window),
				      GTK_FILE_CHOOSER_ACTION_SAVE,
				      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
				      GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
				      NULL);

	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
		char *filename;

		filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));

		/*
		text = g_object_get_data (G_OBJECT (dialog), "text");
		buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (text));
		gtk_text_buffer_get_bounds (buffer, &start, &end);
		comment =
		    gtk_text_buffer_get_text (buffer, &start, &end, FALSE);

		if (turing_fwrite_states (tm->statehead, fname, comment))
			*states_fname = 0;
		else
			strncpy (states_fname, fname, 1024);

		if (prog_message)
			free (prog_message);

		prog_message = strdup (comment);
		*/

		g_free (filename);
	}

	gtk_widget_destroy (dialog);

	/*
	dialog = fsel =
	    gtk_file_selection_new (_("Save gTuring Program File"));

	frame2 = gtk_frame_new (_("Comment"));
	gtk_box_pack_start (GTK_BOX (GTK_FILE_SELECTION (fsel)->main_vbox),
			    frame2, TRUE, TRUE, 0);
	frame = gtk_frame_new (NULL);
	gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_NONE);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 8);
	gtk_container_add (GTK_CONTAINER (frame2), frame);
	scrolled = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (frame), scrolled);
	buffer = gtk_text_buffer_new (NULL);
	text = gtk_text_view_new_with_buffer (buffer);
	gtk_text_view_set_editable (GTK_TEXT_VIEW (text), TRUE);

	if (prog_message)
		gtk_text_buffer_set_text (buffer, prog_message,
					  strlen (prog_message));

	gtk_container_add (GTK_CONTAINER (scrolled), text);

	gtk_widget_show_all (frame2);

	gtk_file_selection_set_filename (GTK_FILE_SELECTION (fsel),
					 states_fname);
	g_object_set_data (G_OBJECT (fsel), "text", text);
	g_signal_connect (GTK_OBJECT
			  (GTK_FILE_SELECTION (fsel)->ok_button),
			  "clicked",
			  GTK_SIGNAL_FUNC (states_fname_callback),
			  (gpointer) TRUE);
	g_signal_connect (GTK_OBJECT
			  (GTK_FILE_SELECTION (fsel)->cancel_button),
			  "clicked", GTK_SIGNAL_FUNC (cancel_callback),
			  NULL);
	gtk_widget_show (fsel);
	*/
}

void
open_callback (GtkWidget *widget, gpointer data)
{
	GtkWidget *dialog;

	dialog = gtk_file_chooser_dialog_new (_("Open a Turing Program File"),
				      GTK_WINDOW (main_window),
				      GTK_FILE_CHOOSER_ACTION_OPEN,
				      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
				      GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
				      NULL);

	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
		char *filename;

		filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));

		if (turing_fread_states (tm, filename)) {
			states_fname = NULL;	/* error */
		} else {
			g_free (states_fname);
			states_fname = g_strdup (filename);

			if (prog_message) {
				g_free (prog_message);
			}

			prog_message = turing_fread_comments (filename);
			view_comment ();
		}

		set_states_fname (filename);

		gconf_client_set_string (gengine, GCONF_PROGRAM,
					 filename, NULL);

		g_free (filename);

	}

	gtk_widget_destroy (dialog);
}

static void
tape_callback (GtkWidget *widget, gpointer data)
{
	gchar *result = NULL;
	
	if (prompt (_("Tape Setting"), _("Please enter the tape:"),
				tape_string, &result)) {

		turing_set_tape (tm, result);
		set_tape (result);

		if (tm->statehead && (*result != 0)) {
			power_do (result);
		} else {
			set_ui_sensitive (FALSE, FALSE, FALSE, FALSE);
		}

		gconf_client_set_string (gengine, GCONF_TAPE,
					 result, NULL);
	}
	g_free (result);
}


void
view_comment_callback (GtkWidget *widget, gpointer data)
{
	view_comment ();
}

void
exit_callback (GtkWidget *widget, gpointer data)
{
	gint width, height;
	
	gtk_window_get_size (GTK_WINDOW (main_window), &width, &height);
	
	gconf_client_set_int (gengine, GCONF_WIDTH, width, NULL);
	gconf_client_set_int (gengine, GCONF_HEIGHT, height, NULL);
	
	gtk_main_quit ();
}

void
playspeed_callback (GtkWidget *widget, gpointer data)
{
	gchar *value;
	gchar *result = NULL;

	value = g_strdup_printf ("%d", speed);
	if (prompt (_("Animation Speed"), _("Miliseconds between steps:"),
				value, &result)) {
		speed = atol (result);
		gconf_client_set_int (gengine, GCONF_SPEED, speed, NULL);
	}
	g_free (value);
	g_free (result);
}


static void
help_callback (GtkWidget *widget, gpointer data)
{
	GError *error = NULL;

	gnome_help_display ("gturing.xml", NULL, &error);

	if (error != NULL) {
		g_warning (error->message);
		g_error_free (error);
	}
}

static void
about_callback (GtkWidget *widget, gpointer data)
{
	/* TODO: Add logo */
	static GtkWidget *about = NULL;
	const gchar *authors[] = { "Arturo Espinosa Aldama",
		/* German has "a" acute and Caamano has tilde over n,
		 * like a' and n~  */
  		_("German Poo-Caamano"),
		NULL
	};
	
	const gchar *documentors[] = { "Arturo Espinosa Aldama", 
		"Alexander Kirillov", NULL };

	if (about != NULL) {
		gtk_window_present (GTK_WINDOW (about));
		return;
	}
	
	/* GdkPixbuf *logo = gdk_pixbuf_new_from_file ("logo.png", NULL); */
	
	about = g_object_new (GTK_TYPE_ABOUT_DIALOG,
	                      "name", "gTuring", 
	                      "version", VERSION,
	                      "copyright", 
			      _
			      ("(c) 1997-2001 Free Software Foundation\n(c) 2001-2005 "
			         /* German has "a" acute and Caamano has tilde over n,
			          * like a' and n~  */
			          "German Poo-Caamano"),
	                      "comments", _("A Turing machine for GNOME"),
			      "authors", authors,
			      "documenters", documentors,
			      "translator-credits", _("translator-credits"),
			      NULL);
	/* g_object_unref (pixbuf); */
	gtk_window_set_transient_for (GTK_WINDOW (about), GTK_WINDOW (main_window));
	gtk_widget_show (about);
}

static void
create_machine (void)
{
	PangoFontDescription *font_desc = NULL;
	gint font_size;
	gchar *font_name;

	font_name = gconf_client_get_string (gengine, GCONF_FONT, NULL);

	if (!font_name) {
	    font_name = g_strdup ("Monospace 10");
	}
	
	font_desc = pango_font_description_from_string (font_name);

	g_assert (font_desc != NULL);

	font_size = pango_font_description_get_size (font_desc);
	pango_font_description_set_size (font_desc, font_size + 4*PANGO_SCALE);
	pango_font_description_set_weight (font_desc, PANGO_WEIGHT_BOLD);
	
	gtk_widget_modify_font (GTK_WIDGET (tapelabel), font_desc);
	gtk_widget_modify_font (GTK_WIDGET (headlabel), font_desc);

	pango_font_description_free (font_desc);
	g_free (font_name);
}

static GtkWidget *
init_interface (void)
{
	static GtkWidget *hpaned;
#ifdef GRAPH_EDITOR
	GtkWidget *graph;
#endif
	GtkWidget *treeview;
	GtkWidget *window;
	GtkWidget *menubar;
	GtkWidget *toolbar;
	GtkActionGroup *action_group;
	GtkUIManager *ui_manager;
	GtkAccelGroup *accel_group;
	GError *error;
	gint width;
	gint height;

	window = gnome_app_new ("gTuring", _("A Turing machine"));

	action_group = gtk_action_group_new ("Actions");
#ifdef ENABLE_NLS
	gtk_action_group_set_translation_domain(action_group, 
		GETTEXT_PACKAGE);
#endif
	gtk_action_group_add_actions (action_group, entries,
		G_N_ELEMENTS (entries), window);

	ui_manager = gtk_ui_manager_new ();
	gtk_ui_manager_insert_action_group (ui_manager, action_group, 0);

	accel_group = gtk_ui_manager_get_accel_group (ui_manager);
	gtk_window_add_accel_group (GTK_WINDOW (window), accel_group);

	error = NULL;
	if (!gtk_ui_manager_add_ui_from_string (ui_manager, 
		    ui_description, -1, &error)) {
		g_message ("building menus failed: %s", error->message);
		g_error_free (error);
		exit (EXIT_FAILURE);
	}

	stop_action = gtk_action_group_get_action (action_group, "Pause");
	play_action = gtk_action_group_get_action (action_group, "Play");
	step_action = gtk_action_group_get_action (action_group, "Step");
	reset_action = gtk_action_group_get_action (action_group, "Reset");

	menubar = gtk_ui_manager_get_widget (ui_manager, "/MainMenu");
	toolbar = gtk_ui_manager_get_widget (ui_manager, "/Toolbar");

	statusline =
	    GNOME_APPBAR (gnome_appbar_new
			  (FALSE, TRUE, GNOME_PREFERENCES_NEVER));
	gnome_app_set_statusbar (GNOME_APP (window),
				 GTK_WIDGET (statusline));

	//vbox = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
	vbox = gtk_vbox_new (FALSE, 0);

	gtk_box_pack_start (GTK_BOX (vbox), menubar, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), toolbar, FALSE, FALSE, 0);

	tapelabel = gtk_label_new (_("Welcome to gTuring"));
	headlabel = gtk_label_new (_("^                 "));
	gtk_box_pack_start (GTK_BOX (vbox), GTK_WIDGET (tapelabel), FALSE,
			    FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), GTK_WIDGET (headlabel), FALSE,
			    FALSE, 0);

	hpaned = gtk_hpaned_new ();

	gtk_container_set_border_width (GTK_CONTAINER (hpaned), GNOME_PAD);

#ifdef GRAPH_EDITOR
	graph = graph_editor_new (&(graph_editor));
	gtk_paned_add1 (GTK_PANED (hpaned), graph);
#endif

	treeview = turing_table_editor_new (&treeview_editor, tm);

	gtk_paned_add2 (GTK_PANED (hpaned), treeview);

	gtk_box_pack_start (GTK_BOX (vbox), hpaned, TRUE, TRUE, 0);
/*	set_save_sens (FALSE);
	set_ui_sensitive (FALSE, FALSE, FALSE, FALSE);*/

	gnome_app_set_contents (GNOME_APP (window), vbox);
	g_signal_connect (GTK_OBJECT (window), "destroy",
			  GTK_SIGNAL_FUNC (exit_callback), NULL);
	
	width  = gconf_client_get_int (gengine, GCONF_WIDTH, NULL);
	height = gconf_client_get_int (gengine, GCONF_HEIGHT, NULL);
	
	width = (width <= 0) ? 350 : width;
	height = (height <= 0) ? 400 : height;

	gtk_window_set_default_size (GTK_WINDOW (window), width, height);
	
	return window;
	// gtk_widget_show_all (main_window);
}

void
init_globals (gchar *filename)
{
	gchar *tape;
	gchar *program = NULL;

	tm = turing_new ();
	turing_table_editor_set_model (treeview_editor, tm);

	tape = gconf_client_get_string (gengine, GCONF_TAPE, NULL);
		
	if (tape != NULL) {
		tape_string = g_strndup (tape, strlen (tape));
		/* set_tape (tape); */
		turing_set_tape(tm, tape);
		g_free (tape);
	}
	
	if (speed < 0) {	/* Was it assigned as argument? */
		speed =
		    gconf_client_get_int (gengine, GCONF_SPEED, NULL);
	}

	if (filename != NULL) {
		program = g_strdup (filename);
	} else {
		program = gconf_client_get_string (gengine, 
			GCONF_PROGRAM, NULL);
	}

	if (turing_fread_states (tm, program)) {
		states_fname = NULL;	/* error */
	} else {
		g_free (states_fname);
		states_fname = g_strdup (program);

		if (prog_message) {
			g_free (prog_message);
		}

		prog_message = turing_fread_comments (program);
	}
	g_free (program);
}

/* The main. */
int
main (int argc, char *argv[])
{
	gchar *filename = NULL; 
	gchar **filenames = NULL; 

	const GOptionEntry n_gturing_options[] = {
		{ "play-speed", 'p', 0, G_OPTION_ARG_INT, &speed,
		 _("Set the animation speed (miliseconds)"), _("ms") },
		{ G_OPTION_REMAINING, 0, 0, G_OPTION_ARG_FILENAME_ARRAY,
		  &filenames, NULL, _("[file]") },
		{ NULL }
	};

	GOptionContext *context;
	GnomeProgram *program;

#ifdef ENABLE_NLS
	setlocale (LC_ALL, "");
	bindtextdomain (GETTEXT_PACKAGE, GTURING_LOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
#endif

	context = g_option_context_new (_("- Show how to use GOption"));
	g_option_context_add_main_entries (context, n_gturing_options,
			GETTEXT_PACKAGE);

	program = gnome_program_init (PACKAGE, VERSION, 
				LIBGNOMEUI_MODULE,
				argc, argv, 
				GNOME_PARAM_GOPTION_CONTEXT, context,
				GNOME_PARAM_APP_DATADIR, DATADIR,
				GNOME_PARAM_NONE);

	/* Process al the remainings arguments, but we only support only
	 * one filename (the first one) */
	if (filenames != NULL) {
		gint num_args;

		num_args = g_strv_length (filenames);
		if (num_args >= 1) {
			filename = g_strdup (filenames[0]);
		}
		g_strfreev (filenames);
		filenames = NULL;
	}

	gconf_init (argc, argv, NULL);
	gengine = gconf_client_get_default ();

	main_window = init_interface ();
	create_machine ();

	init_globals (filename);

	set_tape (tape_string);

	if (states_fname != NULL)
		set_states_fname (states_fname);

	gtk_widget_show_all (main_window);

	gtk_main ();

	g_object_unref (program);
	g_free (filename);

	return 0;
}
