/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* turing_table_editor.h - a Turing machine simulator.
 * Copyright (C) 2001-2002 German Poo-Caaman~o
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __TURING_TABLE_EDITOR_H
#define __TURING_TABLE_EDITOR_H

#include "turing.h"

/* Edit states */
enum
{
	COLUMN_STATE,
	COLUMN_READ,
	COLUMN_WRITE,
	COLUMN_MOVE,
	COLUMN_NEW_STATE,
	COLUMN_COMMENTS,
	COLUMN_EDITABLE,
	COLUMN_MOVE_DATA,
	COLUMN_INDEX,
	NUM_COLUMNS
};

/* Interfaces of tree_editor */
GtkWidget * turing_table_editor_new (GtkTreeView ** tree, turing * tm);
void turing_table_editor_set_model (GtkTreeView * treeview_editor, turing * tm);
void turing_table_editor_animate(GtkTreeView * editor, const turing * tm);

#endif
