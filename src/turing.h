/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* turing.h - a Turing machine simulator.
 * Copyright (C) 1998 The Free Software Foundation
 * Copyright (C) 2001-2002 German Poo-Caaman~o
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __TURING_H
#define __TURING_H

/* The tape is dynamic as it is supposed to be 'infinite'. Check out move_tape
 * to see how it will grow. */
typedef struct _tape
{
	char value;
	struct _tape *next;
	struct _tape *prev;
}
turing_tape;

/* The machine contains a list of states... maybe it could have been faster if
 * I had implemented a graph. */
typedef struct _state
{
	gint index;
	gint no;
	gchar read;
	gchar write;
	gchar move;
	gint new;
	gchar comments[1024];
	
	struct _state *next;
}
turing_state;

typedef struct
{
	gint state;
	gint pos;
	turing_state *statehead;
	turing_state *actualstate;
	turing_tape *tapehead;
	turing_tape *actualtape;
}
turing;

gint turing_max_index(turing * machine);
gint turing_max_state(turing * machine);

turing * turing_new (void);

gint turing_fread_states (turing *machine, gchar *filename);
gchar * turing_fread_comments (gchar *filename);
gint turing_fwrite_states (turing_state *state, gchar *filename, gchar *comment);

gint turing_run_state (turing *machine);
gboolean turing_set_state (turing *machine, turing_state state);
turing_state * new_state (void);
gboolean turing_remove_state (turing * machine, gint index);

void turing_set_tape(turing *machine, const gchar *ptr);
gchar * turing_get_tape(turing *machine);

#endif
