/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* graph_editor.c - a graph editor for gTuring
 * Copyright (C) 2001-2002 German Poo-Caaman~o
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <gnome.h>
#include <libgnomeui/libgnomeui.h>
#include <libgnomeui/gnome-window.h>
#include <libgnomecanvas/libgnomecanvas.h>

#include "turing.h"
#include "gturing.h"
#include "graph_editor.h"

GtkWidget *
graph_editor_new (GtkWidget ** graph_editor)
{
	GtkWidget *scrolled_window;
	GnomeCanvasGroup *root;
	GnomeCanvasGroup *group;

	scrolled_window = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW
					(scrolled_window),
					GTK_POLICY_ALWAYS,
					GTK_POLICY_ALWAYS);
	/*gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW
					     (scrolled_window),
					     GTK_SHADOW_ETCHED_IN);*/
	gtk_scrolled_window_set_placement (GTK_SCROLLED_WINDOW
						(scrolled_window),
						GTK_CORNER_TOP_LEFT);

	(*graph_editor) = gnome_canvas_new_aa ();
	root = gnome_canvas_root (GNOME_CANVAS (*graph_editor));
	/* gtk_widget_set_usize (root, 2048.0, 2048.0); */
	gnome_canvas_set_scroll_region (GNOME_CANVAS (root), -1024.0, -1024.0,
					1024.0, 1024.0);
	
	group = GNOME_CANVAS_GROUP (gnome_canvas_item_new (root,
	   gnome_canvas_group_get_type (),
	   "x", 0.0,
	   "y", 0.0,
	   NULL));
	
	/* background */
	gnome_canvas_item_new (group,
			       gnome_canvas_rect_get_type (),
			       "x1", -1024.0,
			       "y1", -1024.0,
			       "x2", 1024.0,
			       "y2", 1024.0,
			       "fill_color", "white",
			       "outline_color", "white",
			       "width_units", 0.0, NULL);

	gnome_canvas_item_new (root,
			       gnome_canvas_text_get_type (),
			       "text", _("gTuring Graph Editor Canvas based"),
			       "x", 10.0,
			       "y", 10.0,
			       "font", "Sans 12",
			       "anchor", GTK_ANCHOR_N,
			       "fill_color", "black",
			       NULL);
				   
	gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (scrolled_window), (*graph_editor));
	return scrolled_window;
}
