/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* turing_table_editor.c - a Turing machine simulator.
 * Copyright (C) 2001-2002 German Poo-Caaman~o
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <gnome.h>
#include <libgnomeui/libgnomeui.h>

#include "turing.h"
#include "turing_table_editor.h"
#include "gturing.h"

static void turing_table_editor_add_columns (GtkTreeView * treeview,
					     turing * tm);
static void turing_table_editor_add_columns (GtkTreeView * treeview,
					     turing * tm);
static GtkTreeModel *turing_table_editor_model_new (turing * tm);
static GtkTreeView *turing_table_editor_treeview_new (turing * tm);
static void turing_table_editor_append_state (GtkListStore * model,
					      turing_state * s);
static turing_state *turing_table_editor_state_from_row (GtkTreeView *
							 editor,
							 GtkTreeIter *
							 iter);

GdkPixbuf *left_pixbuf = NULL;
GdkPixbuf *right_pixbuf = NULL;

static const char *rightarrow_xpm[] = {
	"20 16 27 1",
	" 	c None",
	".	c #000000",
	"+	c #00FFFF",
	"@	c #00E7E7",
	"#	c #00F5F5",
	"$	c #00C4C4",
	"%	c #009595",
	"&	c #00FDFD",
	"*	c #00F9F9",
	"=	c #00CCCC",
	"-	c #00A2A2",
	";	c #00EFEF",
	">	c #00C7C7",
	",	c #00A8A8",
	"'	c #00DDDD",
	")	c #00A4A4",
	"!	c #009696",
	"~	c #008E8E",
	"{	c #008F8F",
	"]	c #00A5A5",
	"^	c #00C3C3",
	"/	c #007878",
	"(	c #009E9E",
	"_	c #007777",
	":	c #00A7A7",
	"<	c #006969",
	"[	c #007676",
	"                    ",
	"                    ",
	"             ..     ",
	"             .+.    ",
	"             .+@.   ",
	"   ...........#$%.  ",
	"   .&*+++++++#=$=-. ",
	"   .;>=======$$==,,.",
	"   .')!~~~~~{]^=~/. ",
	"   ...........({_.  ",
	"             .:<.   ",
	"             .[.    ",
	"             ..     ",
	"                    ",
	"                    ",
	"                    "
};

static const char *leftarrow_xpm[] = {
	"20 16 29 1",
	" 	c None",
	".	c #000000",
	"+	c #EE0000",
	"@	c #D90000",
	"#	c #CE0000",
	"$	c #C00000",
	"%	c #EA0000",
	"&	c #D70000",
	"*	c #C60000",
	"=	c #C10000",
	"-	c #FF0000",
	";	c #F40000",
	">	c #C40000",
	",	c #CF0000",
	"'	c #BE0000",
	")	c #C30000",
	"!	c #CD0000",
	"~	c #C50000",
	"{	c #CC0000",
	"]	c #AA0000",
	"^	c #BC0000",
	"/	c #B50000",
	"(	c #AC0000",
	"_	c #A70000",
	":	c #8E0000",
	"<	c #B20000",
	"[	c #9A0000",
	"}	c #A20000",
	"|	c #930000",
	"                    ",
	"                    ",
	"     ..             ",
	"    .+.             ",
	"   .+@.             ",
	"  .+#$...........   ",
	" .%&*=@-------;>.   ",
	".+,'=)!~{{{{{{*].   ",
	" .]$^/(_]]]]]]]:.   ",
	"  .]<[...........   ",
	"   .}|.             ",
	"    .:.             ",
	"     ..             ",
	"                    ",
	"                    ",
	"                    "
};

GdkEventButton last_click;

/**
 *turing_table_editor_animate
 *@editor: a treeview editor widget
 *@tm: ptr to the turing machien being run
 *
 *Description: Highlights a row in the treeview editor widget corresponding 
 * to the active transition in the tm.
 */
void
turing_table_editor_animate (GtkTreeView * editor, const turing * tm)
{
	GtkTreeModel *model = gtk_tree_view_get_model (editor);
	GtkTreePath *path;
	GtkTreeSelection *selection = gtk_tree_view_get_selection (editor);
	GtkTreeIter iter;
	gboolean valid;
	gint cstate;
	gchar *cread;

	if (NULL == tm || NULL == tm->actualtape) {
		return;
	}

	/* g_print("selecting state: %d, read: %c\n", 
	   tm->state, tm->actualtape->value); */

	for (valid = gtk_tree_model_get_iter_first (model, &iter);
	     valid; valid = gtk_tree_model_iter_next (model, &iter)) {
		gtk_tree_model_get (model, &iter,
				    COLUMN_STATE, &cstate,
				    COLUMN_READ, &cread, -1);
		if (cstate == tm->state &&
		    cread[0] == tm->actualtape->value) {
			g_free (cread);
			break;
		}
		g_free (cread);
	}

	if (valid) {
		path = gtk_tree_model_get_path (model, &iter);
		gtk_tree_selection_unselect_all (selection);
		gtk_tree_selection_select_path (selection, path);
		gtk_tree_view_scroll_to_cell (editor, path, NULL,
					      FALSE, 0.0, 0.0);
		if (path) {
			gtk_tree_path_free (path);
		}
	} else {
		gtk_tree_selection_unselect_all (selection);
	}
}

/**
 *turing_table_editor_append_state
 *@model: model for the store, if NULL tries to get the model from
 *treeview_editor.
 *@s: ptr to the state to append to the list held in the widget
 *
 *Description: makes a row at the end of the table to represent a new
 *'state' (actually a transition)
 */
void
turing_table_editor_append_state (GtkListStore * model, turing_state * s)
{
	GtkTreeIter iter;

	gchar text[3][2];
	GdkPixbuf *pixbuf;

	text[0][0] = s->read;
	text[0][1] = 0;
	text[1][0] = s->write;
	text[1][1] = 0;
	text[2][0] = s->move;
	text[2][1] = 0;
	pixbuf = (s->move == 'l') ? left_pixbuf : right_pixbuf;

	gtk_list_store_append (model, &iter);
	gtk_list_store_set (model, &iter,
			    COLUMN_STATE, s->no,
			    COLUMN_READ, text[0],
			    COLUMN_WRITE, text[1],
			    COLUMN_MOVE, pixbuf,
			    COLUMN_MOVE_DATA, text[2],
			    COLUMN_NEW_STATE, s->new,
			    COLUMN_COMMENTS, s->comments,
			    COLUMN_EDITABLE, TRUE,
			    COLUMN_INDEX, s->index, -1);
}

/**
 * turing_table_editor_model_new
 * @tm: A Turing's states machine
 *
 * Description: Create a model of a Turing's states machine.
 *
 * Returns: A GtkTreeModel with the turing machine loaded.
 */
static GtkTreeModel *
turing_table_editor_model_new (turing * tm)
{
	GtkListStore *model;
	turing_state *s;

	model = gtk_list_store_new (NUM_COLUMNS, G_TYPE_INT, G_TYPE_STRING,
				    G_TYPE_STRING, GDK_TYPE_PIXBUF,
				    G_TYPE_INT, G_TYPE_STRING,
				    G_TYPE_BOOLEAN, G_TYPE_STRING,
				    G_TYPE_INT);

	/* Fill the model with the machine's states */
	for (s = tm->statehead; s; s = s->next) {
		turing_table_editor_append_state (model, s);
	}
	return GTK_TREE_MODEL (model);
}

/**
 * turing_table_editor_set_model
 * @treeview_editor: A GtkTreeView where is needed update its
 * model
 * @tm: The new Turing's states machines for treeview_editor.
 *
 * Description: Updates the Turing's states machine of a 
 * turing_table_editor (GtkTreeView)
 *
 */
void
turing_table_editor_set_model (GtkTreeView * treeview_editor, turing * tm)
{
	GtkTreeModel *model;

	model = turing_table_editor_model_new (tm);
	gtk_tree_view_set_model (treeview_editor, model);
	g_object_set_data (G_OBJECT (treeview_editor), "turing_machine",
			   tm);
}

/**
 *turing_table_editor_new_state_callback
 *@item: ptr to the menu item that was clicked
 *@data: is a pointer to the editor widget we are acting on
 *
 *Description:  activated when the popum menu for adding a new transition
 *is clicked, adds a new transition into the machine and the table.
 */
static void
turing_table_editor_new_state_callback (GtkMenuItem * item, gpointer data)
{
	GtkListStore *model =
	    GTK_LIST_STORE (gtk_tree_view_get_model
			    (GTK_TREE_VIEW (data)));
	turing_state *state = new_state ();
	turing *machine =
	    g_object_get_data (G_OBJECT (data), "turing_machine");

	state->index = turing_max_index (machine) + 1;
	state->no = turing_max_state (machine) + 1;
	turing_set_state (machine, *state);
	turing_table_editor_append_state (model, state);
	g_free (state);
}

/**
 *turing_table_editor_remove_state_callback
 *@item: the menu item that was clicked to activate this callback
 *@data: a pointer to the treeview editor that contained the popup menu
 *
 *Description: callback that will remove the transition rule over which
 * the user selected the "Remove Transitoion" item in the popup menu.
 */
static void
turing_table_editor_remove_state_callback (GtkMenuItem * item,
					   gpointer data)
{
	GtkTreeView *tree_view = GTK_TREE_VIEW (data);
	GtkTreeModel *model = gtk_tree_view_get_model (tree_view);
	turing *machine =
	    g_object_get_data (G_OBJECT (data), "turing_machine");

	GtkTreePath *path;
	GtkTreeViewColumn *column;
	GtkTreeIter iter;
	gboolean on_row;
	gint cindex;

	on_row = gtk_tree_view_get_path_at_pos (tree_view, last_click.x,
						last_click.y, &path,
						&column, NULL, NULL);
	if (!on_row)
		return;

	gtk_tree_model_get_iter (model, &iter, path);
	gtk_tree_model_get (model, &iter, COLUMN_INDEX, &cindex, -1);
	turing_remove_state (machine, cindex);
	gtk_list_store_remove (GTK_LIST_STORE (model), &iter);
	if (path != NULL)
		gtk_tree_path_free (path);
}

/** 
 *turing_table_editor_popup_handler
 *@widget: the widget that received the signal
 *@event: the mouse-button event
 *@data: ptr to the the editor widget that received the click 
 *
 *Description: If an arrow in the move column is being clicked on, toggles
 *the direction for the transition. A right-click pops up menu to add a new 
 *transition. All other clicks result in the default behavior of selecting
 *cells, etc.
 */
static gint
turing_table_editor_popup_handler (GtkWidget * widget, GdkEvent * event)
{

	GtkMenu *menu;
	GdkEventButton *event_button;

	GtkTreeView *tree_view;
	GtkTreeModel *model;
	GtkTreePath *path;
	GtkTreeViewColumn *column;

	GtkTreeIter iter;
	gchar *cmovedata;
	GdkPixbuf *newpixbuf;
	turing_state *s;
	gboolean on_row;

	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_MENU (widget), FALSE);
	g_return_val_if_fail (event != NULL, FALSE);

	menu = GTK_MENU (widget);

	if (event->type == GDK_BUTTON_PRESS) {
		event_button = (GdkEventButton *) event;
		last_click = *event_button;
		switch (event_button->button) {
		case 3:
			gtk_menu_popup (menu, NULL, NULL, NULL, NULL,
					event_button->button,
					event_button->time);
			return TRUE;
			break;
		case 1:
			tree_view =
			    GTK_TREE_VIEW (gtk_get_event_widget (event));
			model = gtk_tree_view_get_model (tree_view);
			on_row =
			    gtk_tree_view_get_path_at_pos (tree_view,
							   event_button->x,
							   event_button->y,
							   &path, &column,
							   NULL, NULL);

			if (!on_row
			    ||
			    strcmp (gtk_tree_view_column_get_title
				    (column), ("Move")))
				return FALSE;
			gtk_tree_model_get_iter (model, &iter, path);
			gtk_tree_model_get (model, &iter,
					    COLUMN_MOVE_DATA, &cmovedata,
					    -1);
			if (cmovedata[0] == 'l') {
				newpixbuf = right_pixbuf;
				cmovedata[0] = 'r';
			} else {
				newpixbuf = left_pixbuf;
				cmovedata[0] = 'l';
			}
			gtk_list_store_set (GTK_LIST_STORE (model), &iter,
					    COLUMN_MOVE_DATA, cmovedata,
					    COLUMN_MOVE, newpixbuf, -1);
			s = turing_table_editor_state_from_row (tree_view,
								&iter);
			turing_set_state (tm, *s);
			g_free (s);
			if (path != NULL)
				gtk_tree_path_free (path);
			return FALSE;
			break;
		}
	}
	return FALSE;
}

/**
 * turing_table_editor_new
 * @tree: A Widget where GtkTreeView with the columns needed 
 * will be created.
 *
 * Description: Create a GtkScrolledWindow with an empty 
 * GtkTreeView, ready for use it with states of Turing's
 * machine. 
 */
GtkWidget *
turing_table_editor_new (GtkTreeView ** tree, turing * tm)
{
	GtkWidget *scrolled_window;
	GtkMenu *menu;
	/* item for popup menu */
	GtkWidget *item;

	left_pixbuf = gdk_pixbuf_new_from_xpm_data (leftarrow_xpm);
	right_pixbuf = gdk_pixbuf_new_from_xpm_data (rightarrow_xpm);

	scrolled_window = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW
					(scrolled_window),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW
					     (scrolled_window),
					     GTK_SHADOW_ETCHED_IN);

	(*tree) = turing_table_editor_treeview_new (tm);
	gtk_container_add (GTK_CONTAINER (scrolled_window),
			   GTK_WIDGET (*tree));

	menu = GTK_MENU (gtk_menu_new ());

	/* Set up item for adding transitions */
	item = gtk_menu_item_new_with_label (_("New Transition"));
	g_signal_connect (item, "activate",
			  G_CALLBACK
			  (turing_table_editor_new_state_callback), *tree);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	gtk_widget_show (item);

	/* Set up item for removing transitions */
	item = gtk_menu_item_new_with_label (_("Delete Transition"));
	g_signal_connect (item, "activate",
			  G_CALLBACK
			  (turing_table_editor_remove_state_callback),
			  *tree);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	gtk_widget_show (item);


	g_signal_connect_swapped (GTK_OBJECT (*tree), "button_press_event",
				  G_CALLBACK
				  (turing_table_editor_popup_handler),
				  GTK_OBJECT (menu));

	return scrolled_window;
}

/**
 * turing_table_editor_treeview_new
 *
 * Description: Create a GtkTreeView with an empty model.
 *
 */
GtkTreeView *
turing_table_editor_treeview_new (turing * tm)
{
	GtkTreeView *treeview;
	GtkTreeModel *model;

	/* create an empty model */

	model =
	    GTK_TREE_MODEL (gtk_list_store_new
			    (NUM_COLUMNS, G_TYPE_INT, G_TYPE_STRING,
			     G_TYPE_STRING, GDK_TYPE_PIXBUF,
			     G_TYPE_INT, G_TYPE_STRING, G_TYPE_BOOLEAN,
			     G_TYPE_STRING, G_TYPE_INT));

	treeview = GTK_TREE_VIEW (gtk_tree_view_new_with_model (model));
	gtk_tree_view_set_rules_hint (treeview, TRUE);
	gtk_tree_selection_set_mode (gtk_tree_view_get_selection
				     (treeview), GTK_SELECTION_SINGLE);
	turing_table_editor_add_columns (treeview, tm);

	g_object_unref (G_OBJECT (model));

	return treeview;
}

/**
 *@iter: an iterator pointing to a row of the table
 *
 *Description: allocates and returns a new state, with its members 
 *filled in from the row pointed to by iter.
 */
turing_state *
turing_table_editor_state_from_row (GtkTreeView * editor,
				    GtkTreeIter * iter)
{

	turing_state *ts = new_state ();
	GtkTreeModel *model = gtk_tree_view_get_model (editor);
	gint cstate, cnew_state, cindex;
	gchar *cread, *cwrite, *cmove, *ccomments;

	gtk_tree_model_get (model, iter, COLUMN_STATE, &cstate,
			    COLUMN_READ, &cread,
			    COLUMN_WRITE, &cwrite,
			    COLUMN_MOVE_DATA, &cmove,
			    COLUMN_NEW_STATE, &cnew_state,
			    COLUMN_COMMENTS, &ccomments,
			    COLUMN_INDEX, &cindex, -1);

	ts->index = cindex;
	ts->no = cstate;
	ts->read = cread[0];
	ts->write = cwrite[0];
	ts->move = cmove[0];
	ts->new = cnew_state;
	strncpy (ts->comments, ccomments, strlen (ccomments) + 1);
	g_free (cread);
	g_free (cwrite);
	g_free (ccomments);
	return ts;
}

static void
cell_edited (GtkCellRendererText * cell,
	     gchar * path_string, gchar * new_text, gpointer data)
{
	GtkTreeModel *model =
	    gtk_tree_view_get_model (GTK_TREE_VIEW (data));
	GtkTreePath *path = gtk_tree_path_new_from_string (path_string);
	GtkTreeIter iter;
	turing_state ts;
/*	turing *tm;*/

	gint *column;
	gint cstate, cnew_state, cindex;
	gchar *cread, *cwrite, *cmove, *ccomments;

	column = g_object_get_data (G_OBJECT (cell), "column");
	/*tm = g_object_get_data (G_OBJECT (cell), "turing_machine"); */

	gtk_tree_model_get_iter (model, &iter, path);


	switch ((gint) column) {
	case COLUMN_STATE:
	case COLUMN_NEW_STATE:
		{
			gint old_text;

			gtk_tree_model_get (model, &iter, column,
					    &old_text, -1);

			gtk_list_store_set (GTK_LIST_STORE (model), &iter,
					    column, atoi (new_text), -1);
			break;
		}
	default:		/* Change strings is easy */
		{
			gint i;
			gchar *old_text;

			gtk_tree_model_get (model, &iter, column,
					    &old_text, -1);

			i = gtk_tree_path_get_indices (path)[0];

			gtk_list_store_set (GTK_LIST_STORE (model), &iter,
					    column, new_text, -1);
			g_free (old_text);
			break;
		}
	}

	gtk_tree_model_get (model, &iter, COLUMN_STATE, &cstate,
			    COLUMN_READ, &cread,
			    COLUMN_WRITE, &cwrite,
			    COLUMN_MOVE_DATA, &cmove,
			    COLUMN_NEW_STATE, &cnew_state,
			    COLUMN_COMMENTS, &ccomments,
			    COLUMN_INDEX, &cindex, -1);
	g_print ("%d: %d, %c, %c, %c, %d, %s\n", cindex, cstate, cread[0],
		 cwrite[0], cmove[0], cnew_state, ccomments);

	ts.index = cindex;
	ts.no = cstate;
	ts.read = cread[0];
	ts.write = cwrite[0];
	ts.move = cmove[0];
	ts.new = cnew_state;
	strncpy (ts.comments, ccomments, strlen (ccomments) + 1);

	turing_set_state (tm, ts);

	g_free (cread);
	g_free (cwrite);
	g_free (ccomments);

	gtk_tree_path_free (path);
}

/**
 * turing_table_editor_add_columns
 * @treeeview: A GtkTreeView where the columns sill be
 * created (emptys columns)
 *
 * Description: Fill the treeview with empty columns
 *
 */
static void
turing_table_editor_add_columns (GtkTreeView * treeview, turing * tm)
{
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	gchar *text[] = { N_("State"), N_("Read"), N_("Write"), N_("Move"),
		N_("New State"), N_("Comments")
	};
	gint i;

	/* Add each column with title */
	for (i = COLUMN_STATE; i < NUM_COLUMNS - 3; i++) {
		switch (i) {
		case COLUMN_MOVE:
			renderer = gtk_cell_renderer_pixbuf_new ();
			column =
			    gtk_tree_view_column_new_with_attributes (text
								      [i],
								      renderer,
								      "pixbuf",
								      i,
								      NULL);

			break;
		case COLUMN_STATE:
		default:
			renderer = gtk_cell_renderer_text_new ();

			g_signal_connect (G_OBJECT (renderer), "edited",
					  G_CALLBACK (cell_edited),
					  treeview);
			g_object_set_data (G_OBJECT (renderer), "column",
					   (gint *) i);

			g_object_set_data (G_OBJECT (renderer),
					   "turing_machine", tm);

			column =
			    gtk_tree_view_column_new_with_attributes (text
								      [i],
								      renderer,
								      "text",
								      i,
								      "editable",
								      COLUMN_EDITABLE,
								      NULL);
			break;
		}
		gtk_tree_view_append_column (treeview, column);
	}
	/* column = gtk_tree_view_get_column (treeview, 0); */
	/* gtk_tree_view_column_set_sort_column_id (column, COLUMN_STATE); */
}
